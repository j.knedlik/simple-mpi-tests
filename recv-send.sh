mpic++ -o sendrecv.exe -x c++ - << EOF
#include <mpi.h>
int main(int argc, char **argv)
{
  MPI_Init(NULL,NULL);
  int rank;
  int number=-1;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  if (rank == 0) {
  	number = 0;
    	MPI_Send(&number, 1, MPI_INT, 1, 0, MPI_COMM_WORLD);
  } else {
    	MPI_Recv(&number, 1, MPI_INT, 0, 0, MPI_COMM_WORLD,
        MPI_STATUS_IGNORE);
 }
  MPI_Finalize();
  return number;
}
EOF
srun -p hpc_debug  -n 2 -- $PWD/sendrecv.exe 
