mpic++ -o mpitest.exe -x c++ - << EOF
#include <mpi.h>
int main(int argc, char **argv)
{
  MPI_Init(NULL,NULL);
  MPI_Finalize();
  return 0;
}
EOF
srun -p hpc_debug -n 2 -- $PWD/mpitest.exe 
