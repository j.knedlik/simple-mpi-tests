mpic++ -o bcast.exe -x c++ - << EOF
#include <mpi.h>
int main(int argc, char **argv)
{
  MPI_Init(NULL,NULL);
  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Bcast(&rank,1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Finalize();
  return rank;
}
EOF
srun -n 2 -- $PWD/bcast.exe
